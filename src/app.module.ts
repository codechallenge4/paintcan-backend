import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { PaintCanController } from './paint-can/paint-can.controller';
import { PaintCanService } from './paint-can/paint-can.service';
import { ConfigModule } from '@nestjs/config';

@Module({
    imports: [ConfigModule.forRoot()],
    controllers: [AppController, PaintCanController],
    providers: [PaintCanService],
})
export class AppModule {}
