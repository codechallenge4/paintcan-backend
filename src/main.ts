import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
    const app = await NestFactory.create(AppModule, { cors: true });
    const port = process.env.PORT ? process.env.PORT : 80;
    await app.listen(port);
    console.log(`Server is running in ${port}`);
}
bootstrap();
