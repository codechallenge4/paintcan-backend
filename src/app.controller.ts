import { Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
    @Get('status-check')
    statusCheck(): { status: boolean } {
        return { status: true };
    }
}
