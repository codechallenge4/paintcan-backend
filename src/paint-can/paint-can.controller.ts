import { Body, Controller, Post, Res } from '@nestjs/common';
import { Wall } from './wall/wall.model';
import { PaintCanService } from './paint-can.service';

@Controller('paint-can')
export class PaintCanController {
    constructor(private readonly paintCanService: PaintCanService) {}

    @Post('calculate')
    async quantitycalc(@Body() walls: Wall[], @Res() res) {
        let total_area = 0;
        walls.forEach((wall: Wall) => {
            total_area += this.paintCanService.calculateMetters(wall);
        });

        res.json({ total_area });
    }

    @Post('quantity')
    async quantityCalc(@Body() body: { total_area: number }, @Res() res) {
        const total_liters = this.paintCanService.calculateLiters(
            Number(body.total_area),
        );
        res.json({ total_liters });
    }

    @Post('paint-can')
    async paintCanCalculate(
        @Body() body: { total_liters: number },
        @Res() res,
    ) {
        const paint_cans = this.paintCanService.calculatePaintcans(
            body.total_liters,
        );
        res.json({ paint_cans });
    }
}
