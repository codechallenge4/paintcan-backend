type Message = {
    en: string;
    ptbr: string;
};

const messageDictionary: { [key: string]: Message } = {
    height_higher: {
        en: 'The height of walls with a door must be at least 30 centimeters higher than the height of the door.',
        ptbr: 'A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta',
    },
    area_50_percent: {
        en: 'Total Area from Doors and Windows cannot be more than 50% of the wall',
        ptbr: 'O total de área das portas e janelas deve ser no máximo 50% da área de parede',
    },
    square_meters_range: {
        en: 'No wall can have less than 1 square meter or more than 50 square meters.',
        ptbr: 'Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados',
    },
};

// Função para obter mensagem por chave e linguagem
export const getMessage = (key: string): string => {
    const message = messageDictionary[key];

    if (message) {
        return message[process.env.LANGUAGE];
    } else {
        return `Message not found for key: ${key}`;
    }
};
