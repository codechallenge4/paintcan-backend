import { HttpException, Injectable } from '@nestjs/common';
import { Wall } from './wall/wall.model';
import { Door } from './door/door.model';
import { Window } from './window/window.model';
import { getMessage } from './dictionary';

@Injectable()
export class PaintCanService {
    protected paintCans: number[] = [0.5, 2.5, 3.6, 18];

    calculateMetters(wall: Wall): number {
        if (wall.doors > 0 && wall.height <= Door.height + 30) {
            throw new HttpException(getMessage('height_higher'), 401);
        }

        const area = wall.height * wall.width;

        if (area < 10000 || area > 500000) {
            throw new HttpException(getMessage('square_meters_range'), 401);
        }

        let sum_area_doors = 0;
        let sum_area_windows = 0;

        for (let i = 0; i < wall.doors; i++) {
            sum_area_doors += this.calc(Door.width, Door.height);
        }

        for (let i = 0; i < wall.windows; i++) {
            sum_area_windows += this.calc(Window.width, Window.height);
        }

        const sum_area_doors_windows = sum_area_doors + sum_area_windows;

        if (sum_area_doors_windows > area * 0.5) {
            throw new HttpException(getMessage('area_50_percent'), 401);
        }

        return area - (sum_area_doors + sum_area_windows);
    }

    calculateLiters(totalArea: number): number {
        return totalArea / 10000 / 5;
    }

    calculatePaintcans(litros: number): number[] {
        const paintCans = this.paintCans.sort((a, b) => b - a);
        const paintsCanNeeded: number[] = [];
        let rest: number = litros;

        for (const size of paintCans) {
            while (rest >= size) {
                paintsCanNeeded.push(size);
                rest -= size;
            }
        }
        return paintsCanNeeded;
    }

    private calc(width: number, height: number) {
        return width * height;
    }
}
