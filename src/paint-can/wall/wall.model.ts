import { IsNumber } from 'class-validator';

export class Wall {
    @IsNumber()
    width: number;

    @IsNumber()
    height: number;

    @IsNumber()
    windows: number;

    @IsNumber()
    doors: number;
}
