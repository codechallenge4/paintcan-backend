# Use a imagem do Node.js
FROM node

# Define o diretório de trabalho
WORKDIR /app

# Copia os arquivos de package.json e package-lock.json
COPY package*.json ./

# Instala as dependências
RUN npm install

# Copia o código do backend
COPY . .

# Expõe a porta em que a API estará rodando
EXPOSE 8080

# Inicia o aplicativo NestJS
CMD ["npm", "run", "start:dev"]
