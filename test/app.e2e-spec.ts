import { Test, TestingModule } from '@nestjs/testing';
import { HttpException, INestApplication } from '@nestjs/common';
import { AppModule } from './../src/app.module';
import { Wall } from './../src/paint-can/wall/wall.model';
import { PaintCanService } from './../src/paint-can/paint-can.service';
import { Door } from './../src/paint-can/door/door.model';

describe('AppController (e2e)', () => {
    let app: INestApplication;
    let paintCanService: PaintCanService;

    beforeEach(async () => {
        const moduleFixture: TestingModule = await Test.createTestingModule({
            imports: [AppModule],
        }).compile();
        paintCanService = new PaintCanService();
        app = moduleFixture.createNestApplication();
        await app.init();
    });
    describe('calculateMetters', () => {
        it('should calculate the correct area without doors or windows', () => {
            const wall: Wall = {
                height: 200,
                width: 200,
                doors: 0,
                windows: 0,
            };
            const area = paintCanService.calculateMetters(wall);
            expect(area).toBe(40000);
        });
        it('should throw an error if wall height is higher than Door.height + 30 with doors > 0', () => {
            const wall: Wall = {
                height: Door.height + 30 + 1,
                width: 5,
                doors: 1,
                windows: 0,
            };
            expect(() => {
                paintCanService.calculateMetters(wall);
            }).toThrow(HttpException);
        });
        it('should throw an error if area is lower than 10000 or higher than 500000', () => {
            const wall: Wall = {
                height: 100,
                width: 50,
                doors: 0,
                windows: 0,
            };
            expect(() => {
                paintCanService.calculateMetters(wall);
            }).toThrow(HttpException);
        });
        it('should throw an error if sum of doors and windows area is higher than 50% of wall area', () => {
            const wall: Wall = {
                height: 100,
                width: 100,
                doors: 2,
                windows: 3,
            };
            expect(() => {
                paintCanService.calculateMetters(wall);
            }).toThrow(HttpException);
        });
    });
    describe('calculateLiters', () => {
        it('should calculate the correct number of liters', () => {
            const totalArea = 50000;
            const liters = paintCanService.calculateLiters(totalArea);
            expect(liters).toBe(1);
        });
    });

    describe('calculatePaintcans', () => {
        it('should calculate the correct sizes of paint cans', () => {
            const litros = 1.5;
            const paintCans = paintCanService.calculatePaintcans(litros);
            expect(paintCans).toEqual([0.5, 0.5, 0.5]);
        });
    });

    describe('calc', () => {
        it('should calculate the correct area', () => {
            const width = 10;
            const height = 5;
            const area = paintCanService['calc'](width, height);
            expect(area).toBe(50);
        });
    });
});
